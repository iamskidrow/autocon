# Autocon
Autocon is a simple script which uses the most commonly used Recon tools and automates the recon process and saves the report to save time and effort :D  
Make sure you have the given tools installed in path and the wordlists are located in "/usr/share/wordlists" :D  

### Tutorial  
https://www.youtube.com/watch?v=OuShgulnU64

# Usage
./autocon https://www.example.com  
./autocon https://www.example.com everything

# Features  
Subdomain Extractor  
Check for Alive Domains  
Extract URLs from waybackurls  
Filter out keypairs and paths  
Checks for CMS  
Checks for WAF  
Scans for Open Ports  
Checks for Subdomain Takeovers  
Fuzz the url for intresting content discovery  

# Modes  
### Quick Mode [quick]  
Extract waybackurls of the single specified Domain  
Check WAF only for the given Domain  
Scan for Open ports of the given domain only  
Check subdomain takeover on the given domain  
Fuzz for intresting files with quick check wordlist  
  
### Everything mode [everything]  
Extract waybackurls of every domain and subdomain  
Check WAF for every domain and subdomain  
Scan for Open ports of every domain and subdomain  
Check subdomain takeovers for every domain and subdomain  
Fuzz for intresting files with ultimate wordlist  

# Output
Reports will be saved under "reports" folder.  
It will delete previous scan reports available in the reports directory and start a fresh scan

# Tools Used
[wordlists](https://github.com/iamskidrow/wordlists)  
[sublist3r](https://github.com/aboul3la/Sublist3r)  
[subfinder](https://github.com/projectdiscovery/subfinder)  
[httprobe](https://github.com/tomnomnom/httprobe)  
[unfurl](https://github.com/tomnomnom/unfurl)  
[waybackurls](https://github.com/tomnomnom/waybackurls)  
[nmap](https://nmap.org/)  
[wafw00f](https://github.com/EnableSecurity/wafw00f)  
[takeover](https://github.com/m4ll0k/takeover)  
[wfuzz](https://github.com/xmendez/wfuzz)
